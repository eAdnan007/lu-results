<?php
/**
 * This file contains all the functions necessery to run the result script in db server side.
 * 
 * @author Mohaimenul Adnan <mail@eadnan.com>
 * @since 1.0
 */

function write( $data ){
	$f = fopen('log.txt', 'a');
	fwrite( $f, print_r($data, 1)."\n\n" );
}

/**
 * Stops the script execution and sends a failure message to relay.
 */
function lur_error($error, $error_message = null){
	header('Content-Type: application/json');

	$result = array(
		'success' => false,
		'error'   => $error );

	if(null != $error_message) $result['message'] = $error_message;

	echo json_encode( $result );

	die();
}

/**
 * Get the semester name from number
 */
function lur_get_semester_name( $number ){
	if( 1 == $number )	
		$cur_semester_name = "Spring";
	if( 2 == $number )		
		$cur_semester_name = "Summer";
	if( 3 == $number )
		$cur_semester_name = "Fall";

	return $cur_semester_name;
}

/**
 * Get result of the batch for a particular semester.
 */
function lur_get_batch($cur_year, $cur_semester, $adm_year, $adm_semester, $program_name, $alt_names){
	global $db_link;

	$program_name_q = array();
	$program_name_q[] = "Degree=".sql_make_string($program_name);
	foreach( $alt_names as $alt_name ){
		$program_name_q[] = "Degree=".sql_make_string($alt_name);
	}
	$program_name_query = implode( ' OR ', $program_name_q );

	$prev_semester_name_q = array();
	$spring = sql_make_string("Spring");
	$summer = sql_make_string("Summer");
	$fall = sql_make_string("Fall");
	
	$result_till = "(SemisterResult.CYear<".sql_make_string($cur_year)." AND (SemisterResult.Semister=$spring OR SemisterResult.Semister=$summer OR SemisterResult.Semister=$fall))";
	if( 1 == $cur_semester ){
		$result_till .= " OR (SemisterResult.CYear=".sql_make_string($cur_year)." AND SemisterResult.Semister=$spring)";
	}
	else if( 2 == $cur_semester ){
		$result_till .= " OR (SemisterResult.CYear=".sql_make_string($cur_year)." AND (SemisterResult.Semister=$spring OR SemisterResult.Semister=$summer))";
	}
	else if( 3 == $cur_semester ){
		$result_till .= " OR (SemisterResult.CYear=".sql_make_string($cur_year)." AND (SemisterResult.Semister=$spring OR SemisterResult.Semister=$summer OR SemisterResult.Semister=$fall))";
	}

	$prev_semester_name_query = implode(' OR ', $prev_semester_name_q );
	$ad_year = sql_make_string( $adm_year );
	$ad_semester = sql_make_string( lur_get_semester_name( $adm_semester ) );

	$student_details_query = "SELECT     Student.StudentID AS ID, Student.StudentName, SUM(SemisterResult.TPoint) AS total_points, SUM(SemisterResult.CCredit) AS credit
		FROM Student INNER JOIN
		     SemisterResult ON Student.StudentID = SemisterResult.SID
		WHERE     ($program_name_query) AND (Student.AddYear = $ad_year) AND (Student.Semister = $ad_semester) AND ($result_till)
		GROUP BY Student.StudentID, Student.StudentName
		ORDER BY Student.StudentID";

	$students = odbc_exec($db_link, $student_details_query);

	$student_id_query = "SELECT StudentID AS SID
		FROM Student
		WHERE ($program_name_query) AND (AddYear = $ad_year) AND (Semister = $ad_semester)";

	$cources_query = "SELECT SID, CCode as cource_code, CCredit as credit, CTitle as cource_title, GPA as gpa, LG as grade
		FROM SemisterResult
		WHERE 
			(Semister=".sql_make_string(lur_get_semester_name($cur_semester))." AND CYear=".sql_make_string($cur_year).")
			AND SID in ($student_id_query)";

	$response = array();
	while( $student = odbc_fetch_array( $students ) ){
		$student['cources'] = array();
		$response[$student['ID']] = $student;
	}

	$cources = odbc_exec($db_link, $cources_query);

	/**
	 * List of cource codes that has been taken in this semester by the batch.
	 */
	$cource_codes = $cource_credits = array();

	while( $cource = odbc_fetch_array($cources) ){
		$response[$cource['SID']]['cources'][$cource['cource_code']] = $cource;

		if( $cource['credit'] > 0 ) $cource_credits[$cource['cource_code']] = $cource['credit'];

		array_push($cource_codes, $cource['cource_code']);
	}

	$counts = array_count_values($cource_codes);
	arsort($counts);
	$cource_codes = array_keys($counts);

	$response = array(
		'students' => $response,
		'cources'  => array_unique( $cource_codes ),
		'credits'  => $cource_credits );

	return $response;

}

/**
 * Get student object from database
 */
function lur_get_student($student_id){
	global $db_link;

	$student_id = sql_make_string( $student_id ); // Prevent SQL injection.
	// $result      = odbc_prepare($db_link, "SELECT * FROM Student WHERE StudentID=?");
	$result      = odbc_exec($db_link, "SELECT * FROM Student WHERE StudentID=$student_id");
	// $result      = odbc_execute($result, array($student_id));
	$row        = odbc_fetch_array($result);

	return $row;
}

/**
 * Get all results for a particular student
 */
function lur_get_result($student_id){
	global $db_link;

	$student_id = sql_make_string($student_id);
	$sql_query  = "SELECT * FROM SemisterResult WHERE SID=$student_id";
	$row_data   = odbc_exec($db_link, $sql_query);
	// $row_data   = odbc_prepare($db_link, $sql_query);
	// $row_data   = odbc_execute($row_data, array( $studnet_id ) );

	$results = array();
	while($data = odbc_fetch_array($row_data)){
		$results[] = $data;
	}

	return $results;
}

/**
 * Check authorization token.
 * 
 * @param string $request jSon array for the request.
 * @return bool Authorization result
 */
function lur_token_valid($request){
	global $db_link;

	/*
	 * Keep seperate token from request.
	 */
	$token = $request['token'];
	unset($request['token']);

	/*
	 * Receive private key from public key
	 */
	$public_key             = sql_make_string($request['public_key']);
	$sql_query              = "SELECT private_key FROM api_keys WHERE public_key=$public_key";
	$result                 = odbc_exec($db_link, $sql_query);
	$row                    = odbc_fetch_array($result);
	if( !$row ) return false;

	/*
	 * Generate new token with the private key
	 */
	ksort($request);

	$new_token              = md5(serialize($request) . $row['private_key']);


	/*
	 * The newly created token should be same as the token received with the request
	 */
	if($token == $new_token) return true;
	return false;
}

/**
 * Returns quoted string to put in SQL query, provides simple sql injection prevention.
 */
function sql_make_string($sin){
         return "'".str_replace("'","\'",$sin)."'";
}

/**
 * Get token from request array. Always replaces request with get_result
 */
function lur_get_token($request){
	global $db_link;

	/*
	 * Keep seperate token from request.
	 */
	$token = $request['token'];
	unset($request['token']);
	$request['action'] = 'get_result';

	/*
	 * Receive private key from public key
	 */
	$public_key             = sql_make_string($request['public_key']);
	$sql_query              = "SELECT private_key FROM api_keys WHERE public_key=$public_key";
	$result                 = odbc_exec($db_link, $sql_query);
	$row                    = odbc_fetch_array($result);

	if( !$row ) lur_error( 'invalid_key', 'Unknown public key' );
	/*
	 * Generate new token with the private key
	 */
	ksort( $request );
	$new_token              = md5(serialize($request) . $row['private_key']);

	/*
	 * The newly created token should be same as the token received with the request
	 */
	return $new_token;
}
