<?php
/**
 * The responder file. Works as the api interface.
 * 
 * @author Mohaimenul Adnan <mail@eadnan.com>
 * @since 1.0
 */


/**
 * Get application configuration
 */
if(file_exists('config.php')){
	require_once('config.php');
}
else {
	die("Configuration file is missing. 
	     Please rename config_sample.php
	     to config.php and update it with
	     your database configuration.");
}
global $db_link;
/**
 * Create a link to database host
 */
$db_link = odbc_connect(DB_HOST, DB_USER, DB_PASS);
if( !$db_link ) 
	lur_error('cannot_connect', 'Unable to connect to database server. Check your credentials.');


/**
 * Include the file that contains all the functions.
 */
require_once('library.php');

/**
 * Check if the request was received or someone is just loading the page
 */
if(!isset($_POST['request'])) 
	lur_error('no_request', "Nothing requested. Please provide your request through POST");

/**
 * Request received from relay-server
 */
$request = (array) json_decode($_POST['request']);

/**
 * No action other than getting results yet.
 * 
 * This condition is kept to allow future upgradation so that the script can handle the request
 * consistantly on an uniform manner without any change on the relay server script.
 */
if( !in_array( $request['action'], array( 'get_result', 'get_batch_result' ) ) ) lur_error('invalid_action', 'Your request was not valid.' );

/**
 * Validate request token
 */
if(!lur_token_valid($request)) lur_error('invalid_token');

/**
 * Initiating response data.
 */
$response = array();

/**
 * Provide only what was requested.
 */
if( 'get_result' == $request['action'] ){
	/**
	 * Get the student object from database
	 */
	$student = lur_get_student($request['student_id']);
	if( !$student ) lur_error( 'no_student', 'Student not found in database.' );

	$results = lur_get_result($request['student_id']);

	/**
	 * Return json response
	 */
	$response = array(
		'success'     => true,
		'student'     => $student,
		'result'      => $results,
		'server_time' => date(DATE_W3C) );
}
elseif( 'get_batch_result' == $request['action'] ){
	$response = array(
		'success' => true,
		'data'    => lur_get_batch( 
			$request['cur_year'], 
			$request['cur_semester'], 
			$request['adm_year'], 
			$request['adm_semester'], 
			$request['program_name'], 
			$request['alt_names'] ),
		'server_time' => date(DATE_W3C) );

}

header('Content-Type: application/json');
echo json_encode($response);

odbc_close($db_link);