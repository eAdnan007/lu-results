<!DOCTYPE html>
<html ng-app="LUResults">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Result Sheet | Leading University</title>
		<link rel="stylesheet" href="css/loading-bar.min.css">
		<link rel="stylesheet" href="css/jquery-ui.min.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/loading-bar.min.js"></script>
		<script src="js/jquery.inputmask.bundle.js"></script>
		<script src="js/lu-results-module.js"></script>
	</head>
	<body>
		<div id="browser-notice" ng-hide="true">
			<div class="uk-alert uk-alert-danger">
				This webpage is intended for modern browsers and it cannot function on your browser. <br>
				Please enable javascript, update your browser or try another one.
			</div>
		</div>
		<div id="content" style="display:none">
			<div class="uk-flex uk-flex-middle uk-flex-center uk-flex-column container">
				<div class="logo-container">
					<img src="img/logo.png" alt="Leading University">
				</div>
				<form 
					id="get-result" 
					class="uk-form uk-text-center ng-scope uk-form-horizontal" 
					method="post"
					ng-controller="ResultFormCtrl"
					<?php echo isset($_GET['debug_str']) ? 'ng-submit="updateResult(studentID, DOB,\''.$_GET['debug_str'].'\')"':'ng-submit="updateResult(studentID, DOB)"';?>>
					
					<div class="uk-grid uk-grid-preserve uk-grid-small">
						<div class="uk-width-medium-4-10 uk-width-1-1"><input type="text" class="uk-form-large uk-width-1-1" ng-model="studentID" name="student_id" placeholder="Student ID"></div>
						<div class="uk-width-medium-4-10 uk-width-1-1"><input type="text" class="datemask uk-form-large uk-width-1-1" ng-model="DOB" name="dob" placeholder="Date of Birth (yyyy-mm-dd)"></div>
						<div class="uk-width-medium-2-10 uk-width-1-1"><button type="submit" class="uk-button uk-button-danger uk-button-large uk-width-1-1">Result</button></div>
					</div>
					
					<div class="uk-alert uk-alert-danger" ng-if="hasError">{{errorData.message}}</div>

				</form>
				<div id="student-info" class="uk-cover middle-block" ng-controller="StudentInfoCtrl">
					<div class="uk-grid uk-container-center" ng-if="enable">
						<div class="uk-width-medium-1-2">
							<strong class="label">Student ID :</strong>
							{{std.id}}
						</div>
						<div class="uk-width-medium-1-2">
							<strong class="label">Semester :</strong>
							{{std.semester}}
						</div>
						<div class="uk-width-medium-1-4">
							<strong class="label">CGPA :</strong>
							{{std.cgpa}}
						</div>
						<div class="uk-width-medium-1-4">
							<strong class="label">Grade :</strong>
							{{std.grade}}
						</div>
						<div class="uk-width-medium-2-4">
							<strong class="label">Credit Completed :</strong>
							{{std.credit}}
						</div>
						<div class="uk-width-medium-1-1">
							<strong class="label">Name :</strong>
							{{std.name}}
						</div>
						<div class="uk-width-medium-1-1">
							<strong class="label">Degree :</strong>
							{{std.degree}}
						</div>
						<div class="uk-width-medium-1-1">
							<strong class="label">Department :</strong>
							{{std.department}}
						</div>
						<div class="uk-width-medium-1-1" ng-if="std.waived_credit">
							<strong class="label">Waived Credits :</strong>
							{{std.waived_credit}}
						</div>
					</div>
				</div>
				<div id="results" class="uk-cover middle-block" ng-controller="ResultsCtrl">
					<div class="uk-container-center" ng-if="enable">
						<div ng-repeat="(year, semesters) in r" class="uk-width-1-1">
							<div ng-repeat="semester in semesters" class="uk-width-1-1 uk-overflow-container semester">
								<h2>{{semester.name}}</h2>
								<table class="uk-table uk-table-striped uk-table-condensed">
									<thead>
										<tr>
											<th>Course Code</th>
											<th class="uk-hidden-small show-print">Course Title</th>
											<th>Credit</th>
											<th>GPA</th>
											<th>Grade</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th></th>
											<th class="uk-hidden-small show-print"></th>
											<th>{{semester.credit}}</th>
											<th>{{semester.gpa}}</th>
											<th>{{semester.grade}}</th>
										</tr>
									</tfoot>
									<tbody>
										<tr ng-repeat="course in semester.courses">
											<td>{{course.course_code}}</td>
											<td class="uk-hidden-small show-print course-title">{{course.course_title}}</td>
											<td>{{course.credit}}</td>
											<td>{{course.gpa}}</td>
											<td>{{course.grade}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div id="footer" class="uk-text-center">
					Developed and maintained by <a href="http://eadnan.com/" target="_blank">Mohaimenul Adnan</a>, Department of CSE
				</div>
			</div>
		</div>

	</body>
</html>