jQuery(document).ready(function($){
	$('.datemask').inputmask({
		alias: "yyyy-mm-dd"
	})
});

(function(){
	angular.element(document).ready(function() {
		angular.element('#content').css('display', 'block');
	});

	var Results = angular.module('LUResults', ['angular-loading-bar']);
	Results.controller('StudentInfoCtrl', function($scope, $interval, data){
		$interval(function(){
			$scope.std = data.student;
			$scope.enable = data.hasData;
		}, 200);
	});

	Results.controller('ResultsCtrl', function($scope, $interval, data){
		$interval(function(){
			$scope.r = data.results;
			$scope.enable = data.hasData;
		}, 200);
	});

	Results.controller('ResultFormCtrl', function($scope, $interval, data){
		$scope.updateResult = data.updateResult;
		
		$interval(function(){
			$scope.errorData = data.errorData;
			$scope.hasError = data.hasError;
		}, 200);
	});

	Results.factory('data', ['$http', function($http){
		var service = {};
		service.errorData = {};
		service.hasError = false;
		
		service.updateResult = function(sid, dob, debug){
			var ds = '';
			if('undefined' != typeof debug) ds = '&debug_string='+debug;

			$http({
				url: 'grabber.php',
				method: 'POST',
				data: 'student_id='+sid+'&birth_date='+dob+ds,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(content, status){
				if( true == content.success ){
					service.student = content.student;
					service.results = content.results;
					service.errorData = {};
					service.hasError = false;
				}
				else {
					service.student = {};
					service.results = {};
					service.hasError = true;
					service.errorData = content;
				}
				service.hasData = content.success;
			});
		}

		return service;
	}]);
})();
