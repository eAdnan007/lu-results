<?php
header('Content-Type: application/json');
require_once('config.php');

if(
	!isset($_POST['student_id']) || 
	!preg_match("/[0-9A-Za-z]{10,20}/", $_POST['student_id']) ||
	( !isset($_POST['birth_date']) && !lur_debugging() ) ||
	( $_POST['birth_date'] == '' && !lur_debugging() ) ||
	( $_POST['birth_date'] == 'undefined' && !lur_debugging() )
) {
	lur_error('invalid_request', 'Form was not submitted properly.');
}

$request = array(
	'action'     => 'get_result',
	'student_id' => trim( $_POST['student_id'] ),
	'public_key' => PUBLIC_KEY,
	'timestamp'  => time() );

$request['token'] = lur_get_token( $request );

$data = array( 'request' => json_encode($request) );

$row_data = lur_send_post_data(DATA_URL, $data);

if( !($row_data = json_decode($row_data))) lur_error('service_unavailable', 'Unable to get results at this moment.');

if( !isset($row_data->message) && false == $row_data->success ) $row_data->message = 'Error occurred while requesting for result.';

if( $row_data->success == false ) lur_error($row_data->error, $row_data->message);

/**
 * Verify users by birthdate
 */
$bdate_input = strtotime($_POST['birth_date']);
$bdate_dbase = strtotime($row_data->student->Date_of_Birth);
$bdate_deflt = strtotime("1-1-1900"); // Many people does not have their birthdate stored 
if($bdate_input != $bdate_dbase && $bdate_dbase != $bdate_deflt && !lur_debugging())
	lur_error( 'birthdate_mismatch', 'Input verification failed, contact admission office.' );


$response = array(
	'success' => true,
	'student' => array(
		'name'       => $row_data->student->StudentName,
		'id'         => $row_data->student->StudentID,
		'department' => $row_data->student->Department,
		'semester'   => $row_data->student->Semister . '-' . $row_data->student->AddYear,
		'degree'     => $row_data->student->Degree ) );

$results = array();

$waived_credit = $weighted_point = $total_credit = 0;

foreach( $row_data->result as $result){

	// Remove faild and incomplete results
	// if(0.0 == $result->GPA) continue;

	if( 'Courses Waived' == $result->Semister ){
		$waived_credit += $result->CCredit;
		continue;
	}

	$temp = array();
	$temp['course_code']  = $result->CCode;
	$temp['course_title'] = $result->CTitle;
	$temp['grade']        = $result->LG;
	$temp['gpa']          = $result->GPA;
	$temp['credit']       = $result->CCredit;

	$results[$result->CYear][$result->Semister][] = $temp;

	$weighted_point += $temp['gpa'] * $temp['credit'];
	$total_credit += $temp['credit'];
}

$response['student']['cgpa'] = round($weighted_point / $total_credit, 2);
$response['student']['credit'] = $total_credit;
$response['student']['grade'] = lur_get_grade($response['student']['cgpa']);
$response['student']['waived_credit'] = $waived_credit;

/**
 * Sort the semesters
 */
foreach($results as $year => $semesters){
	$seasons = array(
		'spring' => 'Spring',
		'summer' => 'Summer',
		'fall'   => 'Fall' );

	foreach( $seasons as $key => $s_name ){
		if(isset($semesters[$s_name])){
			$courses = $semesters[$s_name];
			$credit = $gpa = 0;
			foreach( $courses as $course ){
				$credit += $course['credit'];
				$gpa += ($course['gpa']*$course['credit']);
			}
			$t = array();
			$t['courses'] = $courses;
			$t['name'] = "$s_name - $year";
			$t['gpa'] = round($gpa/$credit, 2);
			$t['credit'] = $credit;
			$t['grade'] = lur_get_grade(round($gpa/$credit, 2));

			$response['results'][$year][] = $t;
		}
	}
}


/**
 * Include the row data with response to debug if debugging
 */
if(lur_debugging()) $response['row_data'] = $row_data;

echo json_encode($response);

/**
 * Check if the result is being debugged to loose some restrictions in favour of it
 */
function lur_debugging(){
	if(!isset($_POST['debug_string'])) return false;
	
	if(function_exists('password_verify') && password_verify( $_POST['debug_string'], DEBUG_SECRET ) ) return true;
	
	if(!function_exists('password_verify') && DEBUG_SECRET == $_POST['debug_string']) return true;
	
	return false;
}

/**
 * Stops the script execution and sends a failure message to relay.
 */
function lur_error($error, $error_message = null){
	$result = array(
		'success' => false,
		'error'   => $error );

	if(null != $error_message) $result['message'] = $error_message;
	else $result['message'] = 'Unable to process your request at this moment.';

	echo json_encode( $result );

	die();
}

/**
 * Get token from request array. Always replaces request with get_result
 */
function lur_get_token($request){
	ksort( $request );
	
	$new_token              = md5(serialize($request) . PRIVATE_KEY);

	/*
	 * The newly created token should be same as the token received with the request
	 */
	return $new_token;
}

/**
 * Posts to a url and return it's result content
 */
function lur_send_post_data($url, $post) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, TIMEOUT);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

/**
 * Get grade from gpa
 */
function lur_get_grade($gpa){
	if( $gpa == 4 ) $olg = 'A+';
	elseif( $gpa >= 3.75 ) $olg = 'A';
	elseif( $gpa >= 3.50 ) $olg = 'A-';
	elseif( $gpa >= 3.25 ) $olg = 'B+';
	elseif( $gpa >= 3.00 ) $olg = 'B';
	elseif( $gpa >= 2.75 ) $olg = 'B-';
	elseif( $gpa >= 2.50 ) $olg = 'C+';
	elseif( $gpa >= 2.25 ) $olg = 'C';
	elseif( $gpa >= 2.00 ) $olg = 'D';
	else $olg = 'F';

	return $olg;
}

