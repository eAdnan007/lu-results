<?php 
define('DATA_URL', '');
define('PUBLIC_KEY', '');
define('PRIVATE_KEY', '');
define('MAX_PAGE_WIDTH', 1750);
define('PAGE_WIDTH', 1500);
define('TIMEOUT', 10);

global $programs;
/**
 * The array contains all the programs. The array keys are array names and value is an array
 * containing details of the programs. The program details array contain keys: 'alt_name' array
 * of alternate names of the program that can be stored in database, 'department',
 * 'starting_semester' which semester the program was first introduced(1/2/3) and 'starting_year'
 * the year the course was first introduced.
 */
$programs = array(
	'Bachelor of Business Administration (Hons)' => array( 
		'alt_names'           => array( 'Bachelor of Business Administration (Honours)' ),
		'department'          => 'Department of BuA',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'B.Sc. (Hons) in Computer Science & Engineering' => array( 
		'alt_names'           => array(
			'B.Sc. (Honours) in Computer Science & Engineering' ),
		'department'          => 'Department of CSE',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'Bachelor of Laws (Honours)' => array( 
		'department'          => 'Department of Law',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'B.Sc. in Civil Engineering' => array( 
		'department'          => 'Department of Civil Engineering',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'Master of Business Administration (Executive)' => array( 
		'department'          => 'Department of BuA',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'B.Sc. (Honours) in Electrical & Electronic Engineering' => array( 
		'department'          => 'Department of EEE',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'Master of Business Administration' => array( 
		'department'          => 'Department of BuA',
		'starting_semester'   => 2,
		'starting_year'       => 2001 ),
	'Master of Laws' => array( 
		'department'          => 'Department of Law',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'Bachelor of Arts (Honours) in English' => array( 
		'alt_names'           => array( 'Master of Arts in English' ),
		'department'          => 'Department of Law',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'Bachelor of Architecture' => array( 
		'department'          => 'Department of Architecture',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
	'B.Sc. (Honours) in Civil Engineering' => array( 
		'department'          => 'Department of Civil Engineering',
		'starting_semester'   => 1,
		'starting_year'       => 2002 ),
);
