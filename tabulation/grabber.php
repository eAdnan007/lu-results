<?php
header('Content-Type: application/json');

require_once('config.php');

if(
	!isset($_POST['batch']) || 
	!is_numeric($_POST['batch']) ||
	!isset($_POST['semester']) ||
	!in_array( $_POST['semester'], array( 1, 2, 3 ) ) ||
	!isset($_POST['year']) ||
	!isset($_POST['program'] )
) {
	lur_error('invalid_request', 'Form was not submitted properly.');
}

$program = $programs[$_POST['program']];
$admission_semester = lur_get_admission_semester( $_POST['batch'], $_POST['program'] );
$alt_names = array();
if( isset($program['alt_names']) ){
	$alt_names = $program['alt_names'];
	sort($alt_names);
}

/**
 * Request array to send to the server. Make any number integer before sending, otherwise it will
 * generate invalid token error.
 */
$request = array(
	'action'       => 'get_batch_result',
	'cur_semester' => (int)$_POST['semester'],
	'cur_year'     => (int)$_POST['year'],
	'adm_semester' => (int)$admission_semester['semester'],
	'adm_year'     => (int)$admission_semester['year'],
	'program_name' => $_POST['program'],
	'alt_names'    => $alt_names,
	'public_key'   => PUBLIC_KEY,
	'timestamp'    => time() );

$request['token'] = lur_get_token( $request );

$data = array( 'request' => json_encode($request) );


$row_data = lur_send_post_data(DATA_URL, $data);



if( !($row_data = json_decode($row_data) ) ) lur_error('service_unavailable', 'Could not communicate with the server.');

if( !isset($row_data->message) && false == $row_data->success ) $row_data->message = 'Error occurred while requesting for result.';

if( $row_data->success == false ) lur_error($row_data->error, $row_data->message);

$row_data->data->details = array(
	'program'          => $_POST['program'],
	'department'       => $programs[$_POST['program']]['department'],
	'current_semester' => lur_get_semester_name($_POST['semester']),
	'current_year'     => $_POST['year'],
	'semester_no'      => lur_get_current_semester_number(  $_POST['batch'], $_POST['program'], $_POST['semester'], $_POST['year'] ),
	'batch'            => lur_add_ordinal_suffix( $_POST['batch'] ) );

echo json_encode($row_data);

/**
 * Check if the result is being debugged to loose some restrictions in favour of it
 */
function lur_debugging(){
	if(!isset($_POST['debug_string'])) return false;
	
	if(function_exists('password_verify') && password_verify( $_POST['debug_string'], DEBUG_SECRET ) ) return true;
	
	if(!function_exists('password_verify') && DEBUG_SECRET == $_POST['debug_string']) return true;
	
	return false;
}

/**
 * Stops the script execution and sends a failure message to relay.
 */
function lur_error($error, $error_message = null){
	$result = array(
		'success' => false,
		'error'   => $error );

	if(null != $error_message) $result['message'] = $error_message;
	else $result['message'] = 'Unable to process your request at this moment.';

	echo json_encode( $result );

	die();
}

/**
 * Get token from request array. Always replaces request with get_result
 */
function lur_get_token($request){
	ksort( $request );
	
	$new_token = md5(serialize($request) . PRIVATE_KEY);

	return $new_token;
}

/**
 * Posts to a url and return it's result content
 */
function lur_send_post_data($url, $post) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, TIMEOUT);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

/**
 * Get grade from gpa
 */
function lur_get_grade($gpa){
	if( $gpa == 4 ) $olg = 'A+';
	elseif( $gpa >= 3.75 ) $olg = 'A';
	elseif( $gpa >= 3.50 ) $olg = 'A-';
	elseif( $gpa >= 3.25 ) $olg = 'B+';
	elseif( $gpa >= 3.00 ) $olg = 'B';
	elseif( $gpa >= 2.75 ) $olg = 'B-';
	elseif( $gpa >= 2.50 ) $olg = 'C+';
	elseif( $gpa >= 2.25 ) $olg = 'C';
	elseif( $gpa >= 2.00 ) $olg = 'D';
	else $olg = 'F';

	return $olg;
}

/**
 * Get the semester name from number
 */
function lur_get_semester_name( $number ){
	if( 1 == $number )	
		$cur_semester_name = "Spring";
	if( 2 == $number )		
		$cur_semester_name = "Summer";
	if( 3 == $number )
		$cur_semester_name = "Fall";

	return $cur_semester_name;
}

/**
 * Get admission semester by batch number
 */
function lur_get_admission_semester( $batch, $program ){
	global $programs;
	$program = $programs[$program];


	$start_y = $program['starting_year'];
	$start_s = $program['starting_semester'];

	$as = $start_s + $batch;
	$ay = floor($as/3) + $start_y;
	$as %= 3;
	$as++;

	return array( 'year' => $ay, 'semester' => $as );
}

/**
 * Calculate the number of current semester
 */
function lur_get_current_semester_number( $batch, $program, $cur_semester, $cur_year ){
	$adm_semester = lur_get_admission_semester( $batch, $program );

	$y = $cur_year - $adm_semester['year'];

	$s = $cur_semester - $adm_semester['semester'] + $y * 3 + 1;

	return $s;
}

/**
 * Adds ordinal suffix depending to provided number.
 * 
 * Example: 21 becomes 21st, 33 becomes 33rd and 45 becoms 45th.
 */
function lur_add_ordinal_suffix($num){
	$n = $num;
    $num = $num % 100; // protect against large numbers
    if($num < 11 || $num > 13){
         switch($num % 10){
            case 1: return $n.'st';
            case 2: return $n.'nd';
            case 3: return $n.'rd';
        }
    }
    return $n.'th';
}

function write( $data ){
	$f = fopen("log.txt", 'a');
	fwrite($f, print_r($data, 1)."\n\n");
	fclose($f);
}


