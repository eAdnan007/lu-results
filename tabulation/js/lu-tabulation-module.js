(function(){
	var Results = angular.module('LUTabulation', ['angular-loading-bar']);
	Results.controller('TabulationCtrl', function($scope, $http){
		$scope.updateTable = function(batch, semester, year, program){
			$scope.height = 800;
			program = encodeURIComponent(program );
			$http({
				url: 'grabber.php',
				method: 'POST',
				data: 'batch='+batch+'&semester='+semester+'&year='+year+'&program='+program,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).
			success(function(content, status){
				$scope.hasData = content.success;
				if( $scope.hasData ){
					$scope.details = content.data.details;
					$scope.credits = content.data.credits;

					$scope.students = [];
					$scope.columns  = ['Student ID', 'Name'];
					angular.forEach(content.data.cources, function(cource){
						$scope.columns.push(cource);
					});
					$scope.columns.push("Cr. Comp.");
					$scope.columns.push("GPA");
					$scope.columns.push("Total Cr. Comp.");
					$scope.columns.push("CGPA");

					angular.forEach(content.data.students, function(student){
						var t = [];
						t['Student ID'] = student.ID;
						t['Name'] = student.StudentName;

						var semester_credit = 0;
						var semester_total_gpa = 0;

						var c_count = 0;
						angular.forEach(content.data.cources, function(cource){
							if( typeof student.cources[cource] != 'undefined' ){
								c_count++;
								t[cource] = parseFloat(student.cources[cource].gpa).toFixed(2)+' '+student.cources[cource].grade;
								
								semester_credit += parseFloat(student.cources[cource].credit);
								semester_total_gpa    += parseFloat(student.cources[cource].gpa) * parseFloat(student.cources[cource].credit);
							}
							else {
								t[cource] = ' ';
							}
						});

						t['Cr. Comp.'] = semester_credit.toFixed(2);
						t['GPA'] = (semester_total_gpa/semester_credit).toFixed(2);
						if(isNaN(t['GPA'])) t['GPA'] = (0).toFixed(2);
						t['Total Cr. Comp.'] = parseFloat(student.credit).toFixed(2);
						t['CGPA'] = (student.total_points/student.credit).toFixed(2);
						if(isNaN(t['CGPA'])) t['CGPA'] = (0).toFixed(2);
						
						if(c_count) 
							$scope.students.push(t);
					});
				}
			}).
			error(function(){
				$scope.hasData = false;
			});
		}
	});

	Results.filter('dashify', function(){
		return function(text){
			if( typeof text == 'undefined' || ' ' == text || '' == text ){
				text = '-';
			}
			return text;
		}
	});

	Results.filter('addCreditOnColumn', function(){
		return function(columnName, scope){

			if(typeof scope.credits[columnName] != 'undefined'){
				return columnName + ' (' + scope.credits[columnName] + ')';
			}

			return columnName;
		}
	});
})();
