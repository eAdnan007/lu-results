<?php require_once('config.php'); ?>
<!DOCTYPE html>
<html ng-app="LUTabulation">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Result Sheet | Leading University</title>
		<link rel="stylesheet" href="css/loading-bar.min.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/loading-bar.min.js"></script>
		<script src="js/lu-tabulation-module.js"></script>
	</head>
	<body>
		<div id="content" ng-controller="TabulationCtrl">
			<div class="uk-flex uk-flex-center container" id="query-form-container">
				<form
					id="tabulation-query" 
					class="uk-form uk-form uk-margin-remove" 
					ng-submit="updateTable(batch, semester, year, program)">
					<nav class="uk-navbar">

						<span class="uk-navbar-brand">Generate Tabulation</span>

						<div class="uk-navbar-content">
							<label for="batch-semester-season">Batch:</label>
							<input type="text" name="batch_number" id="batch-number" size="4" ng-model="batch" />
						</div>

						<div class="uk-navbar-content">
							<label for="current-semester-season">Current Semester:</label>
							<select name="current_semester_season" id="current-semester-season" ng-model="semester">
								<option value="1">Spring</option>
								<option value="2">Summer</option>
								<option value="3">Fall</option>
							</select>
							<input type="text" name="current_semester_year" id="current-semester-year" size="5" placeholder="YYYY" ng-model="year" />
						</div>

						<div class="uk-navbar-content">
							<select name="program" id="program" ng-model="program">
								<option value="">Select Program</option>
								<?php
								global $programs;
								foreach ($programs as $key => $value): ?>
									<option><?php echo $key; ?></option>
								<?php endforeach ?>
							</select>
						</div>

						<div class="uk-navbar-content">
							<button class="uk-button uk-button-primary">Generate</button>
						</div>

						<div class="uk-navbar-content">
							<input type="text" name="height" id="height" size="4" value="800" />
							<input type="button" id="print" class="uk-button uk-button-default" value="Print">
						</div>
					</nav>
				</form>
			</div>
			<table class="uk-table" id="tabulation-sheet" ng-show="hasData">
				<caption>
					<div class="caption-data">
						<h1>Tabulation Sheet</h1>
						<div class="left">
							<strong>{{details.department}}</strong><br>
							Program: {{details.program}}
						</div>
						<div class="right">
							<big class="batch uk-center">{{details.batch}} Batch</big><br>
							{{details.current_semester}} Semester {{details.current_year}} / Semester No-{{details.semester_no}}
						</div>
						<div class="uk-clearfix"></div>
					</div>
				</caption>
				<thead>
					<tr>
						<th ng-repeat="column in columns">{{column|addCreditOnColumn:this}}</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="student in students">
						<td ng-repeat="column in columns">{{student[column]|dashify}}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="{{columns.length}}">
							<div class="caption-data uk-grid">
								<div class="uk-width-1-3 signature-field">
									<hr>
									Prepared By
								</div>
								<div class="uk-width-1-3 signature-field">
									<hr>
									Checked By
								</div>
								<div class="uk-width-1-3 signature-field">
									<hr>
									Controller of Examinations
								</div>
								<div class="uk-width-1-3 signature-field">
									<hr>
									Member 1, Result checking committee
								</div>
								<div class="uk-width-1-3 signature-field">
									<hr>
									Member 2, Result checking committee
								</div>
								<div class="uk-width-1-3 signature-field">
									<hr>
									Pro Vice Chancellor & Convener, Result checking committee
								</div>
							</div>
						</td>
					</tr>
				</tfoot>
			</table>
			<div class="uk-clearfix"></div>
		</div>
			
		<script>
			/*
			 * HTML: Print Wide HTML Tables
			 * http://salman-w.blogspot.com/2013/04/printing-wide-html-tables.html
			 */
			$(function() {
				$("#print").on("click", function() {
					var tabulation_sheet = $("#tabulation-sheet"),
						tableWidth = tabulation_sheet.outerWidth(),
						maxPageWidth = <?php echo MAX_PAGE_WIDTH; ?>,
						pageWidth = <?php echo PAGE_WIDTH; ?>,
						pageHeight = $('#height').val(),
						pageCount = 1,
						printWrap = $("<div class='printable_sheet'></div>").insertAfter(tabulation_sheet),
						printTableWidth,
						heightBreakpoints = [],
						i,
						printPage,
						defaultTableHeight = $('#tabulation-sheet thead').height();

					var printHeight = $('table caption').height() + $('table tfoot').height();

					$('#tabulation-sheet tbody tr').each(function(j){

						printHeight += $(this).height();

						if( printHeight >= pageHeight ){
							heightBreakpoints.push(j-1);
							printHeight = $(this).height() + $('table caption').height() + $('table tfoot').height();
						}
					});

					heightBreakpoints.push($('#tabulation-sheet tbody tr').length);


					if( tableWidth > maxPageWidth ){
						pageCount = Math.ceil(tableWidth / pageWidth);
					}

					printTableWidth = tableWidth/pageCount

					var startRow = 0;
					angular.forEach(heightBreakpoints, function(endRow){

						for (i = 0; i < pageCount; i++) {
							printPage = $("<div></div>").css({
								"overflow": "hidden",
								"width": printTableWidth+10,
								"page-break-before": i === 0 ? "auto" : "always"
							}).appendTo(printWrap);

							printWrap.width(printTableWidth).css('margin', '0 auto');

							var clone = tabulation_sheet.clone();
							clone.removeAttr("id").appendTo(printPage).css({
								"position": "relative",
								"left": -i * printTableWidth
							}).
							find('.caption-data').css({
								"width": printTableWidth,
								"position": "relative",
								"left": i * printTableWidth
							});
							clone.find('tbody tr').hide().slice(startRow, endRow).show();

						}

						startRow = endRow;
					});

					
					tabulation_sheet.hide();
					window.print();
					tabulation_sheet.show();
					$(".printable_sheet").remove();
				});
			});
		</script>
	</body>
</html>